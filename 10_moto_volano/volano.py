import pylab
from scipy.optimize import curve_fit

#spessore, raggio e densita disco
s = 1.07 #+- 0.05 cm
r = 16.5 #+- 0.1 cm
dR = 0.1
d = 2.70 #+- 0.02 g/cm3

#raggio rocchetto
r1 = 0.5*2.45 #+- 0.05 cm
dr1 = 0.05

#massa, momento di inerzia
massa_volano = pylab.pi*(r**2)*s*d
d_massa_volano = 0.001
I = 0.5*massa_volano*r**2

#sostituire errori, in ordine, dR e dM
dI= pylab.sqrt((massa_volano*r*dR)**2+(0.5*r**2*d_massa_volano)**2)
print ('I = %f +- %f x 10^5' % (I*10**(-5), dI*10**(-5)))

#stima momento della forza di attrito
t, w = pylab.loadtxt('attrito.txt', unpack = True)
#t = t[100:250]
#w = w[100:250]
def f(x,a,b):
	return a*x + b

popt, pcov = curve_fit(f, t, w, pylab.array([-0.3,8.]))
m, b = popt
dm, db = pylab.sqrt(pylab.diagonal(pcov))

print('m = %f +- %f' % (m,dm))
print('b = %f +- %f' % (b,db))

mom_attrito = -m*I
d_mom_attrito = pylab.sqrt((-dm*I)**2+(-m*dI)**2)
print('Momento forza attrito = %f+-%f N*cm' % (mom_attrito, d_mom_attrito))

varianza_w = pylab.sqrt(sum((w-f(t,m,b))**2)/len(w))
print varianza_w
x_quadro = sum(((w-f(t,m,b))/0.017)**2)
print x_quadro

pylab.figure(0)
pylab.subplot(211)
pylab.title('Plot di omega su t senza massa - intervallo [100-250]')
pylab.ylabel('Omega [rad/s]')
pylab.grid(color = 'gray')
pylab.plot(t, w, 'o')
pylab.plot(t, f(t,m,b))
pylab.subplot(212)
pylab.xlabel('Tempo [s]')
pylab.ylabel('Omega data - modello [rad/s]')
pylab.plot(t, w-f(t,m,b), '+')
pylab.grid(color = 'gray')
#pylab.savefig('attrito-datamodello2.pdf')


pylab.figure(1)
pylab.title('Plot di omega su t senza massa')
pylab.xlabel('Tempo [s]')
pylab.ylabel('Omega [rad/s]')
pylab.grid()
pylab.plot(t, w, 'o')
pylab.plot(t, f(t,m,b))
#pylab.savefig('attrito.pdf')


#massa+piattello
m1 = 223.320 #+- 0.001 g
dm1 = 0.001

t1, w1 = pylab.loadtxt('dati.txt', unpack = True)

#intervalli salite-discese
intervalli = [0,13.4,19.7,28.5,33.1,39.1,42.0,45.6,48.,50.5,52.5]

m_teorico1 = (m1*981.*r1-mom_attrito)/(I+m1*r1**2)
dm_teorico1 = pylab.sqrt((((981.*r1)*(I+m1+r1**2)-(m1*981.*r1-mom_attrito))/((I+m1*r1**2)**2)*dm1)**2 + (((m1*981.)*(I+m1+r1**2)-(m1*981.*r1-mom_attrito)*(2*r1))/((I+m1*r1**2)**2)*dr1)**2 + (-1/((I+m1*r1**2)**2)*d_mom_attrito)**2 + (-(m1*981.*r1-mom_attrito)/((I+m1*r1**2)**2)*dI)**2)

m_teorico2 = (-m1*981.*r1-mom_attrito)/(I+m1*r1**2)
dm_teorico2 = pylab.sqrt((((-981.*r1)*(I+m1+r1**2)-(-m1*981.*r1-mom_attrito))/((I+m1*r1**2)**2)*dm1)**2 + (((-m1*981.)*(I+m1+r1**2)-(-m1*981.*r1-mom_attrito)*(2*r1))/((I+m1*r1**2)**2)*dr1)**2 + (-1/((I+m1*r1**2)**2)*d_mom_attrito)**2 + (-(-m1*981.*r1-mom_attrito)/((I+m1*r1**2)**2)*dI)**2)

print ('Valori teorici: m1 = %f +- %f , m2 = %f +- %f' % (m_teorico1, dm_teorico1, m_teorico2, dm_teorico2))
pylab.figure(2)
pylab.title('Plot di omega su t con massa')
pylab.xlabel('Tempo [s]')
pylab.ylabel('Omega [rad/s]')
pylab.grid()
pylab.plot(t1, w1, 'o')

for tstart, tend in zip(intervalli[:-1], intervalli[1:]):
	ttemp = t1[(t1>tstart) & (t1<tend)]
	wtemp = w1[(t1>tstart) & (t1<tend)]
	popt, pcov = curve_fit(f, ttemp, wtemp, (1, 1))
	m, b = popt
	Dm, Db = pylab.sqrt(pcov.diagonal())	
	print('m = %f +- %f' % (m, Dm))
	pylab.plot(ttemp, f(ttemp, popt[0], popt[1]))

#pylab.savefig('volano_fitted.pdf')

#Plot moneta
t2, w2 = pylab.loadtxt('moneta.txt', unpack = True)
pylab.figure(3)
pylab.title('Plot di omega su t senza massa ma con moneta')
pylab.xlabel('Tempo [s]')
pylab.ylabel('Omega [rad/s]')
pylab.grid()
pylab.plot(t2, w2, 'o')
#pylab.savefig('moneta.pdf')

pylab.show()
