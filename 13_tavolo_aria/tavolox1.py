import pylab
import numpy
from scipy.optimize import curve_fit

frame, t, x1, y1, x2, y2 = pylab.loadtxt('tavuno.txt', unpack = True)

m = 47.331
dm = 0.001

Dy = pylab.array(len(t)*[1],'d')
Dx = pylab.array(len(t)*[1],'d')

def fit(x, m, b):
	return m*x+b

### FIRST PLOT FIT ###
tstart_fit = t[t<1.73]
xstart_fit = x1[t<1.73]	
popt, pcov = curve_fit(fit, tstart_fit, xstart_fit, pylab.array([-1, 10]))
m1, b1 = popt
dm1, db1 = pylab.sqrt(pcov.diagonal())
print (m1)
print(b1)
tend_fit = t[t>1.73]
xend_fit = x1[t>1.73]	
popt, pcov = curve_fit(fit, tend_fit, xend_fit, pylab.array([-1, 10]))
m2, b2 = popt
dm2, db2 = pylab.sqrt(pcov.diagonal())

pylab.figure(1)
pylab.title('Plot x1')
pylab.xlabel('t [s]')
pylab.ylabel('x [pixel]')
pylab.grid()
pylab.errorbar(t, x1, fmt='o', yerr=Dy)
pylab.plot(tstart_fit, fit(tstart_fit, m1, b1))
pylab.plot(tend_fit, fit(tend_fit, m2, b2))

pylab.savefig('x1.pdf')

chi1 = sum(((xstart_fit-fit(tstart_fit, m1, b1))/(1))**2)
chi2 = sum(((xstart_fit-fit(tstart_fit, m1, b1))/(0.3))**2)

print ('Check errore associato\n 1px: %.2f \n 0.3px: %.2f \n' % (chi1, chi2))

### SECOND PLOT FIT ###
tstart_fit = t[t<1.73]
ystart_fit = y1[t<1.73]	
popt, pcov = curve_fit(fit, tstart_fit, ystart_fit, pylab.array([-1, 10]))
m3, b3 = popt
dm3, db3 = pylab.sqrt(pcov.diagonal())

tend_fit = t[t>1.73]
yend_fit = y1[t>1.73]	
popt, pcov = curve_fit(fit, tend_fit, yend_fit, pylab.array([-1, 10]))
m4, b4 = popt
dm4, db4 = pylab.sqrt(pcov.diagonal())

pylab.figure(2)
pylab.title('Plot y1')
pylab.xlabel('t [s]')
pylab.ylabel('y [pixel]')
pylab.grid()
pylab.errorbar(t, y1, fmt='o', yerr=Dy)
pylab.plot(tstart_fit, fit(tstart_fit, m3, b3))
pylab.plot(tend_fit, fit(tend_fit, m4, b4))

pylab.savefig('y1.pdf')



print("VELOCITA BLU:\n Prima dell'urto: \n v1_x = %.2f \pm %.2f pixel/s \n v1_y = %.2f \pm %.2f pixel/s \n Dopo l'urto: \n v1_x = %.2f \pm %.2f pixel/s \n v1_y = %.2f \pm %.2f pixel/s " 
	% (m1, dm1, m3, dm3, m2, dm2, m4, dm4))

print("\nQUANTITA DI MOTO BLU:\n Prima dell'urto: \n mv1_x = %.2f \pm %.2f pixel/s \n mv1_y = %.2f \pm %.2f pixel/s \n Dopo l'urto: \n mv1_x = %.2f \pm %.2f pixel/s \n mv1_y = %.2f \pm %.2f pixel/s " 
	% (m*m1, pylab.sqrt((m*dm1)**2+(m1*dm)**2), m*m3, pylab.sqrt((m*dm3)**2+(m3*dm)**2), m*m2, pylab.sqrt((m*dm2)**2+(m2*dm)**2), m*m4, pylab.sqrt((m*dm4)**2+(m4*dm)**2)))



pylab.show()

