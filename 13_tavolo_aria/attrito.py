import pylab
import numpy
from scipy.optimize import curve_fit

frame, t, x1, y1, x2, y2 = pylab.loadtxt('tavuno.txt', unpack = True)
t = t[t<1.73]
x = x1[t<1.73]	


Dy = pylab.array(len(t)*[1],'d')
Dx = pylab.array(len(t)*[1],'d')

def fit(x,m,b,mu, g):
	return b + m*x + 0.5*mu*g*x**2

### FIRST PLOT FIT ###
popt, pcov = curve_fit(fit, t, x, pylab.array([179, 43, 0.1, 400]))
m, b, mu, g = popt
dm, db, mu, g = pylab.sqrt(pcov.diagonal())

pylab.figure(1)
pylab.title('Plot x1')
pylab.xlabel('t [s]')
pylab.ylabel('x [pixel]')
pylab.grid()
pylab.errorbar(t, x, fmt='o', yerr=Dy)
pylab.plot(t, fit(t, m, b, mu, g))


pylab.show()

