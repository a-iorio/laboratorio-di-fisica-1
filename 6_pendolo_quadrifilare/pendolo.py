import pylab
from scipy.optimize import curve_fit

#lettura dati in ingresso
t, T, Tr = pylab.loadtxt('/Users/TheMaker/Desktop/lab/pendolo_quadrifilare/PendoloIorio.txt', unpack = True)
n=300
t = t[n:]
T = T[n:]
Tr = Tr[n:]
#geometria del pendolo
w = 2.0
l = 112.9
d = 118.
#Calcolo di velocita e angolo
v = (w/Tr)*(l/d)
Theta = pylab.arccos(1. - (v**2)/(2*980.*l))

#funzioni di fit
def f_v(x, v0, tau):
    return v0*pylab.exp(-x/tau)

def f_Theta(x, p1, p2):
    return 2*pylab.pi*pylab.sqrt(l/980.)*(1+ p1*(x**2) + p2*(x**4))

print('cossugay')

#fit di V vs t
popt_v, pcov_v = curve_fit(f_v, t, v, pylab.array([500.,90.]))
v0_fit, tau_fit = popt_v
dv0_fit, dtau_fit = pylab.sqrt(pcov_v.diagonal())
print('v0 = %f +- %f cm/s' % (v0_fit, dv0_fit))
print('tau = %f +- %f s' % (tau_fit, dtau_fit))
print('lambda = %f  +- %f s' % (1/tau_fit, dtau_fit*(1/tau_fit**2)))

chi = sum((f_v(t,v0_fit,tau_fit)-v)**2/(0.03)**2)
print ("ChiQuadro: %f per %f gradi di liberta" % (chi, len(t)))

#valori degli ordini
a = (T/(2*pylab.pi)*pylab.sqrt(980./l)-1)*(1/Theta**2)
b = (T/(2*pylab.pi)*pylab.sqrt(980./l)-1-a*Theta**2)*(1/Theta**4)
a_m = sum(a)/len(a)
dev_am = pylab.sqrt(sum((a-a_m)**2)/len(a))
b_m = sum(b)/len(b)

print ('primo ordine = %f +- %f' % (a_m, dev_am))
print ('secondo ordine = %f' % (b_m))

"""

pylab.figure(5)
pylab.subplot(211)
pylab.ylabel('v [cm/s]')
pylab.grid(color = 'gray')
pylab.plot(t, v, '+', t, f_v(t, v0_fit, tau_fit))
pylab.subplot(212)
pylab.xlabel('Tempo [s]')
pylab.ylabel('v data - modello [cm/s]')
pylab.plot(t,f_v(t,v0_fit,tau_fit)-v, 'o')
pylab.grid(color = 'gray')

#plot di V vs t
pylab.figure(1)
pylab.xlabel('Tempo [s]')
pylab.ylabel('v [cm/s]')
pylab.grid(color = 'gray')
pylab.plot(t, v, '+', t, f_v(t, v0_fit, tau_fit))
"""


#plot di T vs Theta e dei residui
pylab.figure(2)
pylab.subplot(211)
pylab.ylabel('Periodo [s]')
pylab.grid(color = 'gray')
pylab.plot(Theta, T, 'o', Theta, f_Theta(Theta, 1./16, 11./3072), 'r', Theta, f_Theta(Theta, 1./16, 0))
pylab.subplot(212)
pylab.xlabel('Angolo [rad]')
pylab.ylabel('Periodo data - modello [ms]')
pylab.plot(Theta, 1000*(T-f_Theta(Theta, 1./16, 11./3072)),'+')
pylab.grid(color = 'gray')
#pylab.savefig('2-plotted.pdf')

"""

#plot di T vs t
pylab.figure(3)
pylab.xlabel('Tempo [s]')
pylab.ylabel('Periodo [s]')
pylab.grid(color = 'gray')
pylab.plot(t, T)
"""


pylab.show()
