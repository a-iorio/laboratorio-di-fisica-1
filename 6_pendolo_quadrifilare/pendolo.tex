\documentclass{article}
\usepackage{siunitx} % Provides the \SI{}{} command for typesetting SI units
\usepackage{amssymb,amsmath}
\usepackage{graphicx} % Required for the inclusion of images
\usepackage{pgf,tikz}
\usetikzlibrary{arrows}
\usepackage{float}
\usepackage{booktabs}
\setlength\parindent{0pt} % Removes all indentation from paragraphs
\usepackage[utf8]{inputenc}
\usepackage[T1]{fontenc}
\usepackage{lmodern} % load a font with all the characters
\usepackage{subfig}

\title{Pendolo quadrifilare} % Title

\author{\textsc{A. Iorio}} % Author name

\date{\today} % Date for the report

\begin{document}

\maketitle % Insert the title, author and date

\begin{center}
\begin{tabular}{l r}
Data esperienza: & 4 Marzo 2014 \\ % Date the experiment was performed
Assistente di laboratorio: & L. Baldini % Instructor/supervisor
\end{tabular}
\end{center}


\section{Obiettivi}
\begin{itemize}
\item Stima della costante di smorzamento $\lambda$ e del tempo di smorzamento associato $\tau = 1/\lambda$ del pendolo
\item Verifica della legge del periodo del pendolo \begin{align}\label{eq:pendulum_taylor}
T = 2\pi\sqrt{\frac{l}{g}} \left( 1 + \frac{1}{16}\theta_0^2 +
\frac{11}{3072}\theta_0^4 + \cdots \right).
\end{align}
\end{itemize}

\section{Apparato sperimentale}
\begin{itemize}
\item un pendolo quadrifilare
\item metro a nastro (risoluzione 1 mm)
\item un computer per acquisizione ed analisi dei dati
\end{itemize}

\begin{figure}[H]
\begin{center}
  \begin{tikzpicture}[scale=0.90]
    \pgfmathsetmacro{\xc}{3}
    \pgfmathsetmacro{\yc}{0}
    \pgfmathsetmacro{\r}{4.5}
    \pgfmathsetmacro{\w}{1.25}
    \pgfmathsetmacro{\h}{0.5}
    \pgfmathsetmacro{\doff}{2.5}
    \pgfmathsetmacro{\rangle}{1}
    \pgfmathsetmacro{\thetazero}{40}
    \node at (0, 0) {};
    \fill[draw=black, fill=lightgray, rotate around={\thetazero:(\xc, \yc)}]%
    (\xc - 0.5*\w, \yc - \r - 0.5*\h) rectangle%
    (\xc + 0.5*\w, \yc - \r + 0.5*\h);
    \draw[rotate around={\thetazero:(\xc, \yc)}]%
    (\xc - 0.075*\w, \yc - \r - 0.5*\h) rectangle%
    (\xc + 0.075*\w, \yc - \r - \h);
    \fill (\xc, \yc - \r - 0.75*\h) circle [radius=0.075];
    \draw[style=densely dashed] (\xc, \yc) -- (\xc, \yc - \r);
    \draw[style=densely dashed] (\xc, \yc - \r - 0.75*\h) --%
    (\xc - \doff, \yc - \r - 0.75*\h);
    \draw[style=densely dashed] (\xc, \yc) -- (\xc - \doff, \yc);
    \draw[style=densely dashed] (\xc - \doff, \yc) --%
    (\xc - \doff, \yc - \r -0.75*\h);
    \node at (\xc - \doff - 0.25, \yc - 0.5*\r - 0.5*\h) {$d$};

    \draw (\xc, \yc) -- (\xc + \r*sin{\thetazero}, \yc - \r*cos{\thetazero});
    \draw[style=densely dashed] (\xc, \yc - \r) arc (270:270+\thetazero:\r);
    \draw[style=densely dashed] (\xc, \yc - \r - 0.75*\h) arc%
    (270:270+\thetazero:1.1*\r);
    \draw[style=densely dashed] (\xc, \yc - \r*cos{\thetazero}) --%
    (\xc + \r*sin{\thetazero}, \yc - \r*cos{\thetazero});
    \draw (\xc, \yc - \rangle) arc (270:270+\thetazero:\rangle);
    \node at (\xc, \yc + 0.25) {$0$};
    \node[anchor=east] at (\xc , \yc - 0.5*\r*cos{\thetazero})%
         {$l\cos\theta_0$};
    \node[anchor=east] at (\xc , \yc - 0.5*\r - 0.5*\r*cos{\thetazero})%
         {$l (1 - \cos\theta_0)$};
    \node at (\xc + 0.5*\r*sin{\thetazero} + 0.5, \yc - 0.5*\r*cos{\thetazero})%
          {$l$};
    \node at (\xc + 0.6, \yc - 1.3) {$\theta_0$};
    \node at (\xc + \r*sin{\thetazero} + 1, \yc - \r*cos{\thetazero}) {$m$};
    \node at (\xc + \r*sin{\thetazero} + 0.5, \yc - \r*cos{\thetazero}-0.5) {$z$};

  \end{tikzpicture}
  \caption{Schematizzazione dell'apparato sperimentale}
  \label{fig:pendolo}
\end{center}
\end{figure}

\section{Richiami teorici}
 Nel caso di smorzamento proporzionale alla velocità ci si aspetta un andamento esponenziale: \begin{align} v_o(t) = v_o(0)e^{-\lambda/t} \end{align} con $\lambda$ costante di smorzamento e il cui tempo di smorzamento associato è $\tau = 1/\lambda$.
L’ampiezza istantanea di oscillazione si può ricavare, assumendo trascurabile la perdita di energia per attrito su un quarto di periodo, dal bilancio energetico
\begin{align}
  mgl(1 - \cos\theta_0) = \frac{1}{2}mv_0^2,
\end{align}
e quindi:
\begin{align}
  \theta_0 = \arccos\left( 1 - \frac{v_0^2}{2gl} \right).
\end{align}
Considerando la velocità finale nel punto più basso corretta di un fattore $\frac{l}{d}$ 
\begin{align}
  v_0 = \frac{w}{t_{\rm T}} \times \frac{l}{d}
\end{align} a causa dello diversa posizione del centro di massa per la presenza della lamina.


\section{Dati raccolti}
\begin{table}[htdp]
\begin{center}
  \begin{tabular}{|c|c|c|} \hline
    $d$ & $l$ & $z$ \\ \hline
    118.0 $\pm$ 0.1 cm & 112.9 $\pm$ 0.1 cm & 2.0 $\pm$ 0.1 cm \\ \hline
  \end{tabular}
  \caption{Misure del pendolo}
\end{center}
\end{table}

\begin{figure}[H]
\begin{center}
  \includegraphics[width=290pt]{1.pdf}
  \caption{Misure di velocità in funzione del tempo}
\end{center}
\end{figure}

\begin{figure}[H]
\begin{center}
  \includegraphics[width=290pt]{2.pdf}
  \caption{Misure del periodo in funzione del angolo di oscillazione}
\end{center}
\end{figure}

\section{Analisi dei dati}
\begin{figure}[H]
\begin{center}
  \includegraphics[width=\textwidth]{1-plotted.pdf}
  \caption{Fit grafico di velocità in funzione del tempo}
\end{center}
\end{figure}

Tramite fit grafico e analitico si è stimato un valore del tempo di smorzamento $$\tau = 451.73 \pm 0.68 s$$ e di $$\lambda = (2.214 \pm 0.003) \times 10^{-3} s^{-1} $$
Notiamo che il modello teorico, in particolare per valori grandi di $v$, non sembra accordarsi al meglio con i dati sperimentali. Proviamo, dunque, ad eseguire lo stesso fit con un campione limitato di dati.

\begin{figure}[H]
\begin{center}
  \includegraphics[width=\textwidth]{plotv-data.pdf}
  \caption{Plot del campione limitato di velocità con relativi residui}
\end{center}
\end{figure}
Effettuando un test del $\chi^2$, ed assumendo come errore sulle velocità il valore medio dei residui $\Delta y \simeq 0.03~cm/s$, otteniamo un valore $S_1 = 70.22$ che corrisponde a $P(\chi^2 < S_1)=60\%$ per un sistema a $\nu = 71 - 2 - 1 = 68$ gradi di libertà.

Abbiamo costruito inoltre il grafico del periodo $T$ in funzione del tempo ed abbiamo notato che, a causa della variazione dell’ampiezza di oscillazione, anch’esso diminuisce col tempo.


\begin{figure}[H]
\begin{center}
  \includegraphics[width=280pt]{3.pdf}
  \caption{Misure del periodo in funzione del tempo}
\end{center}
\end{figure}

Abbiamo infine effettuato un fit grafico del periodo $T$ in funzione dell'ampiezza di oscillazione $\theta_0$ sia utilizzando un'espressione che tenesse conto del 2$^o$ ordine dello sviluppo (in rosso) che del 4$^o$ (in verde). 
\begin{figure}[H]
\begin{center}
  \includegraphics[width=280pt]{2-plotted.pdf}
  \caption{Fit grafico del periodo in funzione dell'ampiezza di oscillazione}
\end{center}
\end{figure}

Abbiamo ricavato inoltre una stima del coefficiente correttivo relativo al 2$^o$ ordine pari a: $$a = (6.31 \pm 0.25) \times 10^{-2}$$ Ci è stato impossibile dare una stima del valore del coefficiente del 4$^o$ ordine dalle misure effettuate.

\section{Conclusioni}
Il modello analizzato per l'andamento della velocità si è dimostrato essere plausibile per valori di velocità non elevati ($v<75$ $cm/s$) come si evince dal test del $\chi^2$ effettuato. Per valori maggiori, invece, la velocità non segue un andamento esponenziale.
Notiamo, inoltre, dalla disposizione dei residui in \textit{Figura 5}, che probabilmente la fotocellula era leggermente spostata al momento dell'acquisizione dei dati per cui sistematicamente registrava il passaggio del pendolo in anticipo o ritardo.

Dalle misure effettuate, inoltre, si è potuto \textit{vedere} fino al 2$^o$ ordine dello sviluppo del periodo $T$ ed il coefficiente correttivo stimato è in accordo con la previsione teorica di $a = 1/16 = 6.25 \times 10^{-2}$.
Ci è stato impossibile calcolare il coefficiente del 4$^o$ ordine dello sviluppo a causa del grado di precisione delle misure ed anche dalla \textit{Figura 7} è evidente come sia preossochè indistinguibile il fit fino al 4$^o$ ordine da quello fino al 2$^o$. L'andamento monotono del grafico dei residui, inoltre, ci indica la presenza di errori sistematici che potrebbero essere, oltre al mal posizionamento della fotocellula, l'erratezza del modello utilizzato o l'eccessiva approssimazione della velocità nel punto più basso come costante. 

\end{document}