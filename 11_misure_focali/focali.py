import pylab
from scipy.optimize import curve_fit

def fit(x, m, b):
        return m*x+b

x, y = pylab.loadtxt('convergente.txt', unpack = True)
x = 1./x
y = 1./y
Dx = x**2*0.2
Dy = y**2*0.2

popt, pcov = curve_fit(fit, x, y, pylab.array([-1, 10]), Dy)
m, b = popt
dm, db = pylab.sqrt(pcov.diagonal())
f = 1./b
df = (1./b**2)*db
print('Focale lente convergente: f = %f \pm %f mm' % (f*10, df*10))

Dy_n = pylab.sqrt(Dy**2+(m*Dx)**2)
popt, pcov = curve_fit(fit, x, y, pylab.array([-1, 10]), Dy_n)
m, b = popt
dm, db = pylab.sqrt(pcov.diagonal())
f = 1./b
df = (1./b**2)*db

print('Focale lente convergente 2: f = %f \pm %f mm' % (f*10, df*10))


Dy_n = pylab.sqrt(Dy_n**2+(m*Dx)**2)
popt, pcov = curve_fit(fit, x, y, pylab.array([-1, 10]), Dy_n)
m, b = popt
dm, db = pylab.sqrt(pcov.diagonal())
f = 1./b
df = (1./b**2)*db

print('Focale lente convergente 3: f = %f \pm %f mm' % (f*10, df*10))



pylab.figure(1)
pylab.subplot(211)
pylab.title('Lente convergente')
pylab.ylabel('1/q [cm]')
pylab.grid()
pylab.plot(x, fit(x, m, b))
pylab.plot(x, fit(x, -1, 0.1), 'r--')
pylab.errorbar(x, y, Dy, Dx, 'o')

pylab.subplot(212)
pylab.xlabel('1/p [cm]')
pylab.ylabel('Data - modello 1/q [cm]')
pylab.grid()
pylab.plot(x, y-fit(x, m, b), '+')
pylab.savefig('convergente.pdf')


x, y = pylab.loadtxt('divergente.txt', unpack = True)
x = 1./x
y = 1./y
Dx = x**2*0.2
Dy = y**2*0.2

popt, pcov = curve_fit(fit, x, y, pylab.array([-1, 10]), Dy)
m, b = popt
dm, db = pylab.sqrt(pcov.diagonal())
f = 1./b
df = (1./b**2)*db

print('Focale lente divergente: f = %f \pm %f mm' % (f*10, df*10))

Dy_n = pylab.sqrt(Dy**2+(m*Dx)**2)
popt, pcov = curve_fit(fit, x, y, pylab.array([-1, 10]), Dy_n)
m, b = popt
dm, db = pylab.sqrt(pcov.diagonal())
f = 1./b
df = (1./b**2)*db

print('Focale lente divergente 2: f = %f \pm %f mm' % (f*10, df*10))


Dy_n = pylab.sqrt(Dy_n**2+(m*Dx)**2)
popt, pcov = curve_fit(fit, x, y, pylab.array([-1, 10]), Dy_n)
m, b = popt
dm, db = pylab.sqrt(pcov.diagonal())
f = 1./b
df = (1./b**2)*db

print('Focale lente divergente 3: f = %f \pm %f mm' % (f*10, df*10))


pylab.figure(2)
pylab.subplot(211)
pylab.title('Lente divergente')
pylab.ylabel('1/q [cm]')
pylab.grid()
pylab.plot(x, fit(x, m, b))
pylab.plot(x, fit(x, -1, -0.05), 'r--')
pylab.errorbar(x, y, Dy, Dx, 'o')

pylab.subplot(212)
pylab.xlabel('1/p [cm]')
pylab.ylabel('Data - modello 1/q [cm]')
pylab.grid()
pylab.plot(x, y-fit(x, m, b), '+')
pylab.savefig('divergente.pdf')



pylab.show()

