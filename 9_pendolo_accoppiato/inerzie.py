import uncertainties as unc
import uncertainties.unumpy as unumpy  
import numpy
import pylab

m_1=unumpy.uarray((389,1.95))
m_2 = unumpy.uarray((85.70,1.09))
d=unumpy.uarray((69.60, 0.05))  
h=unumpy.uarray((496, 1.))  

i1 =0.5*m_1*(d/2)**2+m_1*(h+d/2)**2
di1=unumpy.std_devs(i1)  
i2 = 1./3*m_2*h**2

i = i1+i2

m = unumpy.uarray((473.41, 0.001))
omega = ((m_1+m_2)*9810*h/i)**0.5
omega2 = ((m)*9810*h/i)**0.5


print (omega)
print (omega2)