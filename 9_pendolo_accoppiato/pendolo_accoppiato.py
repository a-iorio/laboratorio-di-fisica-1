import pylab
import numpy

from scipy.optimize import curve_fit

#misure
l = 49.7
dl = 0.1



####################################################################################################

#PENDOLO SINGOLO
t1, x1, t2, x2 = pylab.loadtxt('omega_pendolosingolo.txt', unpack = True)
t2 = t2[25:]
x2 = x2[25:]

omega_t = pylab.sqrt(981./l)
domega_t = 1/(2*omega_t)*(981./l**2)*dl

def fit_singolo(t, a, omega, phi, b):
	return a*numpy.cos(omega*t+phi)+b

initial_values = (140., 4.36, 1., 506.)
popt, pcov = curve_fit(fit_singolo, t2, x2, initial_values)
a, omega, phi, b = popt
da, domega, dphi, db = pylab.sqrt(pcov.diagonal())
periodo = 2.*numpy.pi/omega
dperiodo = (2.*numpy.pi/omega**2)*domega

print('PENDOLO SINGOLO')
print('Ampiezza: %f +- %f; \n Omega: %f +- %f; \n Phi: %f +- %f; \n b = %f +- %f \n Periodo = %f +- %f' % (a, da, omega, domega, phi, dphi, b, db, periodo, dperiodo))
print ('Omega teorico %f +- %f' % (omega_t, domega_t))

pylab.figure(1)
pylab.title('Oscillazione libera di un pendolo')
pylab.xlabel('Tempo [s]')
pylab.ylabel('Posizione [Conteggi di ADC]')
pylab.plot(t2, x2, '+')
#pylab.plot(t1, x1, '+')
pylab.plot(t2, fit_singolo(t2, a, omega, phi, b))
pylab.grid()
#pylab.savefig('oscillazione_libera.pdf')


####################################################################################################

#PENDOLO SINGOLO+SMORZAMENTO
t1, x1, t2, x2 = pylab.loadtxt('smorzamento_pendolosingolo.txt', unpack = True)
t1 = t1[75:]
x1 = x1[75:]

def fit(t, a, omega, phi, b, tau):
    return a*numpy.exp(-t/tau)*numpy.sin(omega*t + phi) + b 

initial_values = (250., 4.48, 1., 459., 40)
popt, pcov = curve_fit(fit, t1, x1, initial_values)
a, omega, phi, b, tau = popt
da, domega, dphi, db, dtau = pylab.sqrt(pcov.diagonal())
periodo = 2.*numpy.pi/omega
dperiodo = (2.*numpy.pi/omega**2)*domega

print('\n PENDOLO SMORZATO')
print('Ampiezza: %f +- %f; \n Omega: %f +- %f; \n Phi: %f +- %f; \n b = %f +- %f; \n Tau = %f +- %f \n Periodo = %f +- %f' % (a, da, omega, domega, phi, dphi, b, db, tau, dtau, periodo, dperiodo))

pylab.figure(2)
pylab.title('Oscillazione smorzata di un pendolo')
pylab.xlabel('Tempo [s]')
pylab.ylabel('Posizione [Conteggi di ADC]')
#pylab.plot(t2, x2, '+')
pylab.plot(t1, x1, '+')
pylab.plot(t1, fit(t1, a, omega, phi, b, tau))
pylab.grid()
#pylab.savefig('oscillazione_smorzata.pdf')


####################################################################################################

#PENDOLI IN FASE
t1, x1, t2, x2 = pylab.loadtxt('fase.txt', unpack = True)
#t1 = t1[75:]
#x1 = x1[75:]


initial_values1 = (100., 4.7, 1., 450., 40)

popt1, pcov1 = curve_fit(fit, t1, x1, initial_values1)
a1, omega1, phi1, b1, tau1 = popt1
da1, domega1, dphi1, db1, dtau1 = pylab.sqrt(pcov1.diagonal())

initial_values2 = (250., 4.7, 1., 550., 40)

popt2, pcov2 = curve_fit(fit, t2, x2, initial_values2)
a2, omega2, phi2, b2, tau2 = popt2
da2, domega2, dphi2, db2, dtau2 = pylab.sqrt(pcov2.diagonal())

periodo1 = 2.*numpy.pi/omega1
dperiodo1 = (2.*numpy.pi/omega1**2)*domega1
periodo2 = 2.*numpy.pi/omega2
dperiodo2 = (2.*numpy.pi/omega**2)*domega2

print('\n PENDOLI IN FASE')
print('Ampiezza: %f +- %f; \n Omega: %f +- %f; \n Phi: %f +- %f; \n b = %f +- %f; \n Tau = %f +- %f; \n Periodo = %f +- %f' % (a1, da1, omega1, domega1, phi1, dphi1, b1, db1, tau1, dtau1, periodo1, dperiodo1))
print('Ampiezza: %f +- %f; \n Omega: %f +- %f; \n Phi: %f +- %f; \n b = %f +- %f; \n Tau = %f +- %f; \n Periodo = %f +- %f' % (a2, da2, omega2, domega2, phi2, dphi2, b2, db2, tau2, dtau2, periodo2, dperiodo2))


pylab.figure(4)
pylab.title('Oscillazioni in fase di due pendoli')
pylab.xlabel('Tempo [s]')
pylab.ylabel('Posizione [Conteggi di ADC]')
pylab.plot(t1, x1+b2-b1, '+')
pylab.plot(t2, x2, '+')
pylab.plot(t1, fit(t1, a1, omega1, phi1, b1, tau1)+b2-b1, linewidth=1, color='blue')
pylab.plot(t2, fit(t2, a2, omega2, phi2, b2, tau2), linewidth=1, color='red')
pylab.grid()

#pylab.savefig('fase.pdf')


####################################################################################################

#PENDOLI IN CONTROFASE
t1, x1, t2, x2 = pylab.loadtxt('controfase.txt', unpack = True)
#t1 = t1[75:]
#x1 = x1[75:]


initial_values1 = (75., 4.45, 1., 445., 40)

popt1, pcov1 = curve_fit(fit, t1, x1, initial_values1)
a1, omega1, phi1, b1, tau1 = popt1
da1, domega1, dphi1, db1, dtau1 = pylab.sqrt(pcov1.diagonal())

initial_values2 = (75., 4.45, 1., 555., 40)

popt2, pcov2 = curve_fit(fit, t2, x2, initial_values2)
a2, omega2, phi2, b2, tau2 = popt2
da2, domega2, dphi2, db2, dtau2 = pylab.sqrt(pcov2.diagonal())

periodo1 = 2.*numpy.pi/omega1
dperiodo1 = (2.*numpy.pi/omega1**2)*domega1
periodo2 = 2.*numpy.pi/omega2
dperiodo2 = (2.*numpy.pi/omega**2)*domega2

print('\n PENDOLI IN CONTROFASE')
print('Ampiezza: %f +- %f; \n Omega: %f +- %f; \n Phi: %f +- %f; \n b = %f +- %f; \n Tau = %f +- %f; \n Periodo = %f +- %f' % (a1, da1, omega1, domega1, phi1, dphi1, b1, db1, tau1, dtau1, periodo1, dperiodo1))
print('Ampiezza: %f +- %f; \n Omega: %f +- %f; \n Phi: %f +- %f; \n b = %f +- %f; \n Tau = %f +- %f; \n Periodo = %f +- %f' % (a2, da2, omega2, domega2, phi2, dphi2, b2, db2, tau2, dtau2, periodo2, dperiodo2))


pylab.figure(5)
pylab.title('Oscillazioni in controfase di due pendoli')
pylab.xlabel('Tempo [s]')
pylab.ylabel('Posizione [Conteggi di ADC]')
pylab.plot(t1, x1+b2-b1, '+')
pylab.plot(t2, x2, '+')
pylab.plot(t1, fit(t1, a1, omega1, phi1, b1, tau1)+b2-b1, linewidth=1, color='blue')
pylab.plot(t2, fit(t2, a2, omega2, phi2, b2, tau2), linewidth=1, color='red')
pylab.grid()
#pylab.savefig('controfase.pdf')


####################################################################################################

#PENDOLI BATTIMENTI
t1, x1, t2, x2 = pylab.loadtxt('battimenti.txt', unpack = True)
#t1 = t1[75:]
#x1 = x1[75:]

def battimenti(t, a, omegap, omegab, phi1, phi2, b, tau):
	return 2*a*numpy.exp(-t/tau)*(numpy.cos(omegap*t+0.5*(phi1+phi2))*numpy.cos(omegab*t+0.5*(phi1-phi2)))+b


initial_values1 = (175., 4.5, 0.07, 0.,  0., 450., 60)

popt1, pcov1 = curve_fit(battimenti, t1, x1, initial_values1)
a1, omegap1, omegab1, phi1, phi12, b1, tau1 = popt1
da1, domegap1, domegab1, dphi1, dphi12, db1, dtau1 = pylab.sqrt(pcov1.diagonal())


initial_values2 = (150., 4.5, 0.07, 1.57, 1., 540., 5)

popt2, pcov2 = curve_fit(battimenti, t2, x2, initial_values2)
a2, omegap2, omegab2, phi2, phi22, b2, tau2 = popt2
da2, domegap2, domegab2, dphi2, dphi22, db2, dtau2 = pylab.sqrt(pcov2.diagonal())

print('\n BATTIMENTI')

"""
print('Ampiezza: %f +- %f; \n Omega: %f +- %f; \n Phi: %f +- %f; \n b = %f +- %f; \n Tau = %f +- %f; \n Periodo = %f +- %f' % (a1, da1, omega1, domega1, phi1, dphi1, b1, db1, tau1, dtau1, periodo1, dperiodo1))
print('Ampiezza: %f +- %f; \n Omega: %f +- %f; \n Phi: %f +- %f; \n b = %f +- %f; \n Tau = %f +- %f; \n Periodo = %f +- %f' % (a2, da2, omega2, domega2, phi2, dphi2, b2, db2, tau2, dtau2, periodo2, dperiodo2))
"""

print('1: Omega_p = %f +- %f; Omega_b = %f +- %f' % (omegap1, domegap1, omegab1, domegab1))

print('2: Omega_p = %f +- %f; Omega_b = %f +- %f' % (omegap2, domegap2, omegab2, domegab2))

pylab.figure(6)
pylab.subplot(211)
pylab.title('Battimenti di due pendoli')
pylab.ylabel('Posizione [Conteggi di ADC]')
pylab.plot(t1, x1, '+', color='green')
#pylab.plot(t2, x2, '+', color='blue')
pylab.plot(t1, battimenti(t1, a1, omegap1, omegab1, phi1, phi12, b1, tau1), linewidth=1, color='red')
#pylab.plot(t2, battimenti(t2, a2, omegap2, omegab2, phi2, phi22, b2, tau2), linewidth=1, color='red')
pylab.grid()

pylab.subplot(212)
pylab.ylabel('Data - Modello')
pylab.xlabel('Tempo [s]')
pylab.plot(t1, x1-battimenti(t1, a1, omegap1, omegab1, phi1, phi12, b1, tau1), '+', color='blue')

pylab.grid()
#pylab.savefig('battimenti_1.pdf')
pylab.show()