
\documentclass{article}
\usepackage{siunitx} % Provides the \SI{}{} command for typesetting SI units
\usepackage{amssymb,amsmath}
\usepackage{graphicx} % Required for the inclusion of images
\usepackage{pgf,tikz}
\usetikzlibrary{arrows}
\usepackage{float}
\usepackage{booktabs}
\setlength\parindent{0pt} % Removes all indentation from paragraphs
\usepackage[utf8]{inputenc}
\usepackage[T1]{fontenc}
\usepackage{lmodern} % load a font with all the characters

\title{Oscillazioni accoppiate} % Title

\author{\textsc{A. Iorio}} % Author name

\date{\today} % Date for the report

\begin{document}

\maketitle % Insert the title, author and date

\begin{center}
\begin{tabular}{l r}
Data esperienza: & 18 Marzo 2014 \\ % Date the experiment was performed
Assistente di laboratorio: & L. Baldini % Instructor/supervisor
\end{tabular}
\end{center}

\section{Obiettivi}
\begin{itemize}
\item studio del moto di due pendoli accoppiati
\item studio del fenomeno dei battimenti
\end{itemize}


\section{Apparato sperimentale}
\begin{itemize}
\item due pendoli accoppiati attraverso una molla
\item due smorzatori (galleggianti da pesca)
\item sistema di acquisizione
\end{itemize}
In particolare i pendoli erano costituiti da un'asta a sezione quadrata a cui era attaccato un cilindro sottile. 

\section{Richiami teorici}
Posto $x_A = x_1+x_2$ e $x_B = x_1-x_2$, con $x_1$ e $x_2$ le ascisse dei due pendoli, le equazioni del moto del sistema diventano:
$$\begin{cases}
\ddot{x}_A+{\omega_0}^2 x_A = 0 \\
\ddot{x}_B+({\omega_0}^2+2k/m) x_B = 0
\end{cases}$$

Che hanno soluzioni:
\begin{align}
x_A (t) = A\cos{(\omega_f t+\phi_1)} \\
x_B (t) = B\cos{(\omega_c t+\phi_2)}
\end{align}

Risostituendo i valori di $x_1$ e $x_2$ si ottiene:
$$\begin{cases}
x_1 (t) = \frac{1}{2}[A\cos{(\omega_f t+\phi_1)}+B\cos{(\omega_c t+\phi_2)}]\\
x_2 (t) = \frac{1}{2}[A\cos{(\omega_f t+\phi_1)}-B\cos{(\omega_c t+\phi_2)}]
\end{cases}$$

Che ha soluzioni per A=0 e B=0:
\begin{align}
x_1=x_2=A\cos{(\omega_f t+\phi_1)} && x_1=-x_2=B\cos{(\omega_c t+\phi_1)}
\end{align}
Che corrispondono ai moti in fase e controfase dei pendoli.

Se si sposta uno dei pendoli lasciando l'altro fermo, ovvero se si ha:
\begin{align}
x_1(0)=A_0, ~ \dot{x}_1(0)=0; && x_2(0)=0, ~ \dot{x}_2(0)=0;
\end{align}
Il moto risultante è dato dalla somma, con uguali ampiezze, dei due modi normali, ovvero, posto: \begin{align}
\omega_p = \frac{\omega_f+\omega_c}{2} && \omega_b = \frac{\omega_c-\omega_f}{2} && \phi = \frac{(\phi_2 + \phi_1)}{2} && \varphi = \frac{(\phi_2 - \phi_1)}{2} 
\end{align}

Le equazioni del moto dei due pendoli diventano:
\begin{align}
  x_i(t) & = 2A_0 \cos(\omega_p t + \phi) \cos(\omega_b t + \varphi)
\end{align}

\section{Dati raccolti}
Non potendo pesare separatamente l'asta ed il cilindro, ipotizziamo il cilindro fatto di ottone e l'asta di alluminio. Vedremo in seguito se questa ipotesi di lavoro risulterà verificata.
\\ \\
Pendolo:
\begin{itemize}
\item massa totale: $m = 473.41 \pm 0.001~g$
\end{itemize}
Cilindro:
\begin{itemize}
\item diametro: $d = 69.60 \pm 0.05~mm$
\item spessore: $s = 12.20 \pm 0.05~mm$
\item densità ottone: $\rho_1 = 8.4~g/cm^3$
\end{itemize}
asta a sezione quadrata:
\begin{itemize}
\item lato: $a = 8.00 \pm 0.05~mm$
\item altezza: $l = 496 \pm 1~mm$
\item densità alluminio: $\rho_2 = 2.7~g/cm^3$
\end{itemize}


\section{Analisi dei dati}

\subsection{Oscillazioni di un pendolo singolo}
La massa $m_1$ del cilindro e la massa $m_2$ dell'asta risultano:
\begin{gather*}
m_1 = \rho_1 s \pi (d/2)^2 = 390  \pm 2~g\\
m_2 = \rho_2 a^2 h = 86 \pm 1~g
\end{gather*}

Da cui la massa totale: 
\begin{gather*}
m' = m_1+m_2 = 476 \pm 3 ~g
\end{gather*}

Il momento d'inerzia del pendolo rispetto all'estremità vincolata è dato dalla somma dei momenti di inerzia del cilindro (calcolato con il teorema H-S) e dell'asta.
\begin{gather*}
I_1 = \frac{1}{2}m_1 (d/2)^2+m_1(l+d/2)^2\\
I_2 = \frac{1}{3} m_2 l^2
\end{gather*}
Quindi:
\begin{gather*}
I_z = I_1+I_2 = (1.169 \pm 0.007) \times 10^5~g~cm^3
\end{gather*}

\subsubsection{Oscillazione libera}
\begin{figure}[H]
\begin{center}
  \includegraphics[width=280pt]{oscillazione_libera.pdf}
\end{center}
\end{figure}

Il valore teorico della pulsazione di un pendolo singolo, senza lo smorzatore, risulta:
\begin{align}
    \omega_t = \sqrt{\frac{m g l}{I_z}} \simeq 4.446 \pm 0.005 ~rad/s
\end{align}
Effettuando un fit al calcolatore dei nostri dati abbiamo ottenuto un valore di $\omega_0$ pari a: \begin{align} \omega_0 = 4.440 \pm 0.001 ~rad/s
 \end{align}
 a cui corrisponde un periodo 
 \begin{align}
 T_0=1.4149 \pm 0.0003~ s
 \end{align}


\subsubsection{Oscillazione smorzata}
\begin{figure}[H]
\begin{center}
  \includegraphics[width=300pt]{oscillazione_smorzata.pdf}
\end{center}
\end{figure}

Inserendo ora lo smorzatore, tramite un fit al calcolatore dei nostri dati, abbiamo stimato un tempo di decadimento $\tau$ pari a:
\begin{align}
\tau = 39.1 \pm 0.2~s
\end{align}
una pulsazione angolare \begin{align} \omega'_0: 4.4298 \pm 0.0001~rad/s \end{align}

ed un periodo 
\begin{align}
 T'_0 = 1.4184 \pm 0.0001~s
\end{align}


\subsection{Pendoli in fase e controfase}
Tramite fit analitico, abbiamo stimato i valori delle pulsazioni angolari dei due pendoli in fase che risultano rispettivamente:
\begin{align}
    \omega_{f_1} = 4.4425 \pm 0.0001~rad/s;\\
    \omega_{f_2} = 4.4445 \pm 0.0001~rad/s;
\end{align} 

Analogamente i valori delle pulsazioni angolari in controfase risultano:
\begin{align}
    \omega_{c_1} = 4.5839 \pm 0.0007~rad/s;\\
    \omega_{c_2} = 4.5846 \pm 0.0005~rad/s;
\end{align}

Di seguito riportiamo i relativi plot con fit, nei quali abbiamo allineato le oscillazioni dei pendoli in modo da rendere più evidente l'andamento in fase e controfase.

\begin{figure}[H]
\begin{center}
  \includegraphics[width=\textwidth]{fase.pdf}
\end{center}
\end{figure}

\begin{figure}[H]
\begin{center}
  \includegraphics[width=\textwidth]{controfase.pdf}
\end{center}
\end{figure}

\subsection{Battimenti}
Dalle stime fatte in precedenza, ci aspettiamo dei valori per le pulsazioni angolari portante e modulante pari a:

\begin{align}
\omega_{p_1} = 4.5132 \pm 0.0003~rad/s && \omega_{p_2} = 4.5145 \pm 0.0002~rad/s\\
\omega_{b_1} = 0.0707 \pm 0.0003~rad/s && \omega_{b_2} = 0.0700 \pm 0.0002~rad/s
\end{align}


Stimandoli dai nostri dati, tramite fit analitico, otteniamo per le due oscillazioni:
\begin{align}
\omega_{p_1} = 4.5148 \pm 0.0001~rad/s && \omega_{p_2} = 4.5147 \pm 0.0001~rad/s\\
\omega_{b_1}  = 0.0702 \pm 0.0001~rad/s && \omega_{b_2} = 0.0707 \pm 0.0001~rad/s
\end{align}



\begin{figure}[H]
\begin{center}
  \includegraphics[width=\textwidth]{battimenti.pdf}
\end{center}
\end{figure}

Di seguito riportiamo il plot dei dati fittati per entrambi i pendoli con relativo plot dei residui.

\begin{figure}[H]
\begin{center}
  \includegraphics[width=290pt]{battimenti_1.pdf}
\end{center}
\end{figure}

\begin{figure}[H]
\begin{center}
  \includegraphics[width=290pt]{battimenti_2.pdf}
\end{center}
\end{figure}


\section{Conclusioni}

L'ipotesi di lavoro che il pendolo fosse formato da un cilindro in ottone e un'asta in alluminio risulta confermata dalla compatibilità tra la massa totale pesata del pendolo e la massa stimata.

Abbiamo verificato che il valore della pulsazione del pendolo singolo non smorzato è compatibile con il valore previsto dalla teoria. 

Inoltre, il periodo del pendolo non smorzato differisce da quello smorzato per un coefficiente additivo dell'ordine $\sim 10^{-3}$ s. 

In entrambi i pendoli, nei moti in fase e controfase, è verificata l'ipotesi per cui $\omega_f \sim \omega_0$ e $\omega_c > \omega_f$. 

Nei battimenti, come si nota dal plot dei residui, il fit analitico delle oscillazioni non sembra descrivere appieno la ricchezza dei dati: l'andamento delle oscillazioni non sembra propriamente esponenziale e le non linearità nella misura della posizione fanno sì che la forma dell'oscillazione misurata non sia esattamente sinusoidale. 
Lo stesso errore stimato dal fit sulle pulsazioni angolari portante e modulante può risultare non totalmente affidabile. Non ci stupisce, quindi, un non perfetto riscontro tra le previsioni e le stime ottenute di $\omega_p$ e $\omega_b$ - che infatti si discostano $2.5 - 4~\sigma$ , non giudichiamo gravi queste discrepanze e concludiamo che i valori sperimentali hanno un discreto riscontro con quelli teorici.

\end{document}