import pylab
from scipy.optimize import curve_fit

def f(x, a, b):
        return a*x+b

y, x = pylab.loadtxt('/Users/TheMaker/Desktop/lab/indici_rifrazione/plexiglass.txt', unpack = True)
Dx = pylab.array(len(x)*[0.1],'d')
Dy = pylab.array(len(y)*[0.1],'d')
pylab.figure(1)
pylab.title('Indice di rifrazione del plexiglass')
pylab.xlabel('$R\\sin(\\theta_r)$ $[cm]$')
pylab.ylabel('$R\\sin(\\theta_i)$ $[cm]$')
pylab.grid()
pylab.errorbar(x, y, Dx, Dy, 'o', color='black')

popt, pcov = curve_fit(f, x, y, pylab.array([1.,0.]))
a, b = popt
da, db = pylab.sqrt(pcov.diagonal())
print('Plexiglass: b = %f +- %f compatibile con 0?' % (b, db))

def f1(x, a):
        return a*x

popt, pcov = curve_fit(f1, x, y, pylab.array([1.]), Dy)
print('Plexiglass: n = %f +- %f' % (popt, pylab.sqrt(pcov.diagonal())))

pylab.plot(x, f1(x, a), color='black')
pylab.savefig('rifrazione.pdf')

pylab.show()

