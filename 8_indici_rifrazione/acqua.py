import pylab
from scipy.optimize import curve_fit


p, q1, q2 = pylab.loadtxt('/Users/TheMaker/Desktop/lab/indici_rifrazione/diottro.txt', unpack = True)
Dp = pylab.array(len(p)*[0.1],'d')

p = p-43+1.79
q = (q1+q2)/2
q = q - 1.79

Dq = (q2-q1)/2

# Plot di 1/q vs 1/p
pylab.figure(1)
pylab.title('Indice di rifrazione dell\'acqua')
pylab.xlabel('$1/p$ [$1/cm$]') 
pylab.ylabel('$1/q$ [$1/cm$]') 
pylab.grid(color = 'gray')
pylab.errorbar(1./p, 1/q, xerr=Dp/(p*p), yerr=Dq/(q*q), fmt='o', color='black' )

# Fit con una retta - nota che le incertezze sono ignorate!
def f(x, a, b):
    return a*x + b

popt, pcov = curve_fit(f, 1./p, 1/q, pylab.array([-1.,1.]), Dq)
a, b       = popt
da, db     = pylab.sqrt(pcov.diagonal())
print('Acqua: n = %f +- %f' % (a, da))

pylab.plot(1./p, f(1./p, a, b), color='black' )


pylab.savefig('rifrazione_acqua.pdf')


pylab.show()
