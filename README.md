Tutte le relazioni sono state scritte da Andrea Iorio (https://sites.google.com/site/iorioa94/) nell'A.A. 2013/2014 (facoltà di Fisica - Pisa).
Si possono liberamente scaricare, modificare ed usare come meglio si crede. 
Per gli studenti di Fisica consiglio vivamente di non abusarne, ma di usarle come guida per la stesura delle *proprie* relazioni.
Le relazioni *non* dovrebbero contenere molti errori: si prega comunque di leggere i commenti nei file .tex per maggiori informazioni su cose che andrebbero riviste/modificate. 
Per altre informazioni scrivimi a themaker.94[at]gmail[dot]com.