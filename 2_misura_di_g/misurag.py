import warnings
warnings.filterwarnings("ignore", category=UserWarning) 
import pylab
import numpy
import uncertainties as unc
import uncertainties.unumpy as unumpy 
from scipy.optimize import curve_fit

def fit(x, m, b):
        return m*x+b

m1t1, m2t2, m3t3, m4t4 = pylab.loadtxt('dati.txt', unpack = True)
m1, m2, m3, m4 = m1t1[:1], m2t2[:1], m3t3[:1], m4t4[:1]
t1, t2, t3, t4 = m1t1[1:], m2t2[1:], m3t3[1:], m4t4[1:]

#eseguo le medie sui tempi con relativo errore
t1m, t2m, t3m, t4m = numpy.mean(t1), numpy.mean(t2), numpy.mean(t3), numpy.mean(t4)
dt1m, dt2m, dt3m, dt4m = numpy.std(t1), numpy.std(t2), numpy.std(t3), numpy.std(t4)

#costruisco i relativi array con le SINGOLE oscillazioni (10 originali)
t = numpy.array([t1m/10, t2m/10, t3m/10, t4m/10], 'd')
Dt = numpy.array([dt1m/10, dt2m/10, dt3m/10, dt4m/10], 'd')
masse = numpy.array([m1[0], m2[0], m3[0], m4[0]], 'd')
Dmasse = pylab.array([0.004, 0.002, 0.001, 0.001],'d')

#printo una sintesi dei dati
print('Masse')
print(masse)
print('Errore masse')
print(Dmasse)
print('Tempi medi')
print(numpy.round(t, 2))
print('Errore tempi medi')
print(numpy.round(Dt, 2))

print('Tempi^2 medi')
print(numpy.round(t**2, 2))
print('Errore tempi^2 medi')
print(numpy.round(2*t*Dt, 2))


popt, pcov = curve_fit(fit, masse, t**2, pylab.array([1., 1.]), 2*t*Dt)
m, b = popt
dm, db = pylab.sqrt(pcov.diagonal())
k = 4*(numpy.pi)**2/m
dk = (4*(numpy.pi)**2/m**2)*dm
print('Costante elastica: k = %.2f \pm %.2f N/m' % (k*10**-3, dk*10**-3))
print('Intercetta: b = %.2f \pm %.2f' % (b, db))

pylab.figure(1)
pylab.title('Plot di t^2 su m')
pylab.xlabel('m [g]')
pylab.ylabel('t^2 [s^2]')
pylab.grid()
pylab.errorbar(masse, t**2, (2*t)*Dt, Dmasse, 'o')
pylab.plot(masse, fit(masse,m,b))
pylab.savefig("1.pdf")



masse_2, l = pylab.loadtxt('allungamenti.txt', unpack = True)
Dl = pylab.array([0.2]*len(l),'d')
Dmasse_2 = pylab.array([0.001, 0.004, 0.003, 0.003, 0.002, 0.002, 0.001, 0.002, 0.001, 0.001],'d')

popt, pcov = curve_fit(fit, masse_2, l, pylab.array([1., 1.]), Dl)
m, b = popt
dm, db = pylab.sqrt(pcov.diagonal())
g = m*k
dg = pylab.sqrt((k*dm)**2+(m*dk)**2)

print('Acc. gravita: g = %.1f \pm %.1f m/s^2' % (g*10**-2, dg*10**-2))
print('Intercetta: b = %.2f \pm %.2f' % (b, db))

pylab.figure(2)
pylab.title('Plot di l su m')
pylab.xlabel('m [g]')
pylab.ylabel('l [cm]')
pylab.grid()
pylab.errorbar(masse_2, l, Dl, Dmasse_2, 'o')
pylab.plot(masse_2, fit(masse_2,m,b))
pylab.savefig("2.pdf")

pylab.show()