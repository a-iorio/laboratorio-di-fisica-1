import pylab
from scipy.optimize import curve_fit

"""
#RELAZIONI UTILI
h, d, m
h, l, m2
h, a, m3
d, m
v_cilindro = h*pylab.pi()*(d/2)**2
v_prisma  = h*3*2*a**2*pylab.sqrt(3)
"""

def fit(x, m, b):
        return m*x+b

def fit_s(x, m, b, c):
        return m*x**c+b


x, Dx, y, Dy = pylab.loadtxt('densita.txt', unpack = True)

pylab.figure(1)
pylab.title('Plot di V su m per tutti i pesetti')
pylab.xlabel('V [mm^3]')
pylab.ylabel('m [g]')
pylab.grid()
pylab.errorbar(x, y, Dy, Dx, 'o')
pylab.savefig("11.pdf")



x, Dx, y, Dy = pylab.loadtxt('primometallo.txt', unpack = True)
popt, pcov = curve_fit(fit, x, y, pylab.array([1, 0]), Dy)
m, b = popt
dm, db = pylab.sqrt(pcov.diagonal())

print('Densita rho = %f \pm %f kg/m^3' % (m*10**6, dm*10**6))
print('Intercetta (=0?) = %.2f \pm %.2f g/mm^3' % (b, db))

pylab.figure(2)
pylab.title('Plot di V su m per il primo gruppo')
pylab.xlabel('V [mm^3]')
pylab.ylabel('m [g]')
pylab.grid()
pylab.errorbar(x, y, Dy, Dx, 'o')
pylab.plot(x, fit(x,m,b))
pylab.savefig("22.pdf")

x, Dx, y, Dy = pylab.loadtxt('secondometallo.txt', unpack = True)
popt, pcov = curve_fit(fit, x, y, pylab.array([1, 0]), Dy)
m, b = popt
dm, db = pylab.sqrt(pcov.diagonal())

print('Densita rho = %f \pm %f kg/m^3' % (m*10**6, dm*10**6))
print('Intercetta (=0?) = %.2f \pm %.2f g/mm^3' % (b, db))

pylab.figure(3)
pylab.title('Plot di V su m per il secondo gruppo')
pylab.xlabel('V [mm^3]')
pylab.ylabel('m [g]')
pylab.grid()
pylab.errorbar(x, y, Dy, Dx, 'o')
pylab.plot(x, fit(x,m,b))
pylab.savefig("33.pdf")


x, Dx, y, Dy = pylab.loadtxt('sfere.txt', unpack = True)
popt, pcov = curve_fit(fit_s, x, y, pylab.array([1, 0, 3]), Dy)
m, b, c = popt
dm, db, dc = pylab.sqrt(pcov.diagonal())

print('m rho = %f \pm %f kg/m^3' % (m*10**6, dm*10**6))
print('b (=0?) = %.2f \pm %.2f g/mm^3' % (b, db))
print('c (=3?) = %.2f \pm %.2f' % (c, dc))

pylab.figure(4)
pylab.title('Plot di R su V per le sferette')
pylab.xlabel('R [mm]')
pylab.ylabel('V [mm^3]')
pylab.xscale('log')

pylab.yscale('log')

pylab.grid()
pylab.errorbar(x, y, Dy, Dx, 'o')
pylab.plot(x, fit_s(x,m,b,c))

pylab.savefig("44.pdf")

pylab.show()