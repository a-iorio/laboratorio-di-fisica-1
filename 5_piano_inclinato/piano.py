import warnings
warnings.filterwarnings("ignore", category=UserWarning) 
import pylab
import numpy
import uncertainties as unc
import uncertainties.unumpy as unumpy 
from scipy.optimize import curve_fit

def fit(x, m, b):
        return m*x+b

angoli = pylab.loadtxt('angoli.txt', unpack = True)
theta = numpy.mean(angoli)
Dtheta = numpy.std(angoli)
print("Theta = %.2f \pm %.2f gradi" % (theta, Dtheta))

d1t1, d2t2, d3t3, d4t4, d5t5, d6t6, d7t7 = pylab.loadtxt('dati.txt', unpack = True)
d1, d2, d3, d4, d5, d6, d7 = d1t1[:1], d2t2[:1], d3t3[:1], d4t4[:1], d5t5[:1], d6t6[:1], d7t7[:1]
t1, t2, t3, t4, t5, t6, t7 = d1t1[1:], d2t2[1:], d3t3[1:], d4t4[1:], d5t5[1:], d6t6[1:], d7t7[1:]

#eseguo le medie sui tempi con relativo errore
t1m, t2m, t3m, t4m, t5m, t6m, t7m = numpy.mean(t1), numpy.mean(t2), numpy.mean(t3), numpy.mean(t4), numpy.mean(t5), numpy.mean(t6), numpy.mean(t7)
dt1m, dt2m, dt3m, dt4m, dt5m, dt6m, dt7m = numpy.std(t1), numpy.std(t2), numpy.std(t3), numpy.std(t4), numpy.std(t5), numpy.std(t6), numpy.std(t7)

#costruisco i relativi array
t = numpy.array([t1m, t2m, t3m, t4m, t5m, t6m, t7m], 'd')
Dt = numpy.array([dt1m, dt2m, dt3m, dt4m, dt5m, dt6m, dt7m], 'd')
d = numpy.array([d1[0], d2[0], d3[0], d4[0], d5[0], d6[0], d7[0]], 'd')
Dd = pylab.array(len(d)*[0.3],'d')

#printo una sintesi dei dati
print('Distanze')
print(d)
print('Errore distanze')
print(Dd)
print('Tempi medi')
print(numpy.round(t, 2))
print('Errore tempi medi')
print(numpy.round(Dt, 2))

#previsioni teoriche
t_teorico = pylab.sqrt(18*d/(5*981*numpy.sin(theta*numpy.pi/180)))
print('Tempi teorici')
print(numpy.round(t_teorico, 2))

#propoago gli errori sulle previsioni, usato il pacchetto unumpy
theta_prop = unumpy.uarray((theta*numpy.pi/180,Dtheta*numpy.pi/180))
d_prop = unumpy.uarray((d,Dd))
t_teorico_prop = unumpy.sqrt(18*d_prop/(5*981*unumpy.sin(theta_prop)))
Dt_teorico = unumpy.std_devs(t_teorico_prop)  

print('Errori tempi teorici')
print(numpy.round(Dt_teorico, 2))


#fitto
popt, pcov = curve_fit(fit, d, t**2, pylab.array([1.,1.]), (2*t)*Dt)
m, b       = popt
dm, db     = pylab.sqrt(pcov.diagonal())
print('a = %.3f \pm %.3f' % (m*2, dm*2))
print('intercetta b = %.3f \pm %.3f (compatibile con 0?)' % (b, db))


a_teorico = (5./18*981*numpy.sin(theta*numpy.pi/180))**-1
#propoago gli errori sulle previsioni, usato il pacchetto unumpy
a_teorico_prop = 18./(5*981*unumpy.sin(theta_prop))
Da_teorico = unumpy.std_devs(a_teorico_prop)  

print('a teorico = %.3f \pm %.3f' % (a_teorico*2, Da_teorico*2))

def teorica(s, theta):
		return 18*s/(5*981*numpy.sin(theta*numpy.pi/180))
grid = numpy.linspace(0,150, 10)

pylab.figure(1)
pylab.title('Plot di d su t^2')
pylab.xlabel('d [cm]')
pylab.ylabel('t^2 [s^2]')
pylab.grid()
pylab.errorbar(d, t**2, (2*t)*Dt, Dd, 'o')
pylab.plot(d, fit(d, m, b))
#pylab.savefig("1.pdf")



pylab.figure(2)
pylab.subplot(211)
pylab.title('Plot di d su t^2 con previsioni')
pylab.ylabel('t^2 [s^2]')
pylab.grid()
pylab.errorbar(d, t**2, (2*t)*Dt, Dd, 'o')
#pylab.plot(d, fit(d, m, b))
#con previsioni
pylab.errorbar(d, t_teorico**2, (2*t_teorico)*Dt_teorico, Dd, 'o')
#pylab.plot(grid, teorica(grid, theta), 'r--', color='black')

pylab.subplot(212)
pylab.grid()
pylab.ylabel('Data-modello t^2 [s^2]')
pylab.xlabel('d [cm]')
pylab.plot(d, t**2-t_teorico**2,'+')
#pylab.savefig("2.pdf")


def fit_s(x,m,b,c):
	return m*x**c + b

popt, pcov = curve_fit(fit_s, d, t, pylab.array([1., 0, 2]), Dt)
m, b, c = popt
dm, db, dc = pylab.sqrt(pcov.diagonal())

print('m  = %f \pm %f' % (m, dm))
print('b (=0?) = %.2f \pm %.2f ' % (b, db))
print('c (=2?) = %.2f \pm %.2f' % (c, dc))

pylab.figure(3)
pylab.title('Plot di d su t^2 - Carta bilogaritmica')
pylab.xlabel('d [cm]')
pylab.ylabel('t^2 [s^2]')
pylab.grid()
pylab.xscale('log')
pylab.yscale('log')
pylab.errorbar(d, t, Dt, Dd, 'o')
pylab.plot(d,fit_s(d,m,b,c))
pylab.savefig('3.pdf')
pylab.show()