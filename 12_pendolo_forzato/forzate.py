import pylab
from scipy.optimize import curve_fit
import numpy

x, Dx, y, Dy = pylab.loadtxt('plotted.txt', unpack = True)
y = pylab.sqrt(y**2)



def fit(omega, F, omega_0, gamma):
	return F/(pylab.sqrt((omega_0**2-omega**2)**2+4*(gamma**2)*(omega**2)))


popt, pcov = curve_fit(fit, x, y, pylab.array([1, 4.5, 1]), Dy)
F, omega_0, gamma  = popt
dF, domega_0, dgamma = pylab.sqrt(pcov.diagonal())

chisquare = sum(((y-fit(x, F, omega_0, gamma))/Dy)**2)
print ("Chi quadro con errori del fit: %.1f per un sistema a %.f-?? gradi di liberta" % (chisquare, len(y)))
#forse Dy meglio circa simeq 3
chisquare = sum(((y-fit(x, F, omega_0, gamma))/3.3)**2)
print ('Chi quadro con errore a posteriori: %.1f per un sistema a %.f-?? gradi di liberta' % (chisquare, len(y)))

grid = numpy.linspace(1,7,200)

pylab.figure(1)
pylab.title('Plot risonanza')

pylab.subplot(211)
pylab.grid()
pylab.errorbar(x, y, Dy, Dx, 'o')
pylab.plot(grid, fit(grid, F, omega_0, gamma))
pylab.ylabel('Ampiezza')
pylab.subplot(212)
pylab.grid()
pylab.ylabel('Ampiezza Data - Modello')

pylab.plot(x, y-fit(x, F, omega_0, gamma), '+')
pylab.xlabel('Omega [rad/s]')

#pylab.savefig('fit.pdf')

print('Omega_0 = %.4f \pm %.4f' % (omega_0, domega_0))
print('Gamma = %.4f \pm %.4f' % (gamma, dgamma))


pylab.show()



