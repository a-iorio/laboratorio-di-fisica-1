import pylab
from scipy.optimize import curve_fit
import numpy

x, y = pylab.loadtxt('dati/143.txt', unpack = True)

def fit(x, C, omega, phi, Q):
	return C*numpy.cos(omega*x+phi)+Q


popt, pcov = curve_fit(fit, x, y, pylab.array([200, 4.6, 1, 460]))
C, omega, phi, Q  = popt
dC, domega, dphi, dQ = pylab.sqrt(pcov.diagonal())

pylab.figure(1)
pylab.title('Fit oscillazione 1')
pylab.grid()
pylab.plot(x, y, '+')
pylab.plot(x, fit(x,C,omega,phi, Q))
pylab.xlabel('Tempo [s]')
pylab.ylabel('Posizione')

pylab.savefig('ex1.pdf')
print("%.6f, %.6f" % (omega, domega))
print("%.6f, %.6f" % (C, dC))


pylab.show()

