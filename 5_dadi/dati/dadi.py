import pylab
import numpy
import math

n_dadi = 10
n_lanci = 10000

valori, occorrenze = pylab.loadtxt('dado10_10000.txt', unpack = True)
mu = sum(occorrenze*valori)/n_lanci
varianza_campione = sum(occorrenze*((valori - mu)**2))/(n_lanci - 1)
sigma = pylab.sqrt(varianza_campione)


def gauss_integral(mu, sigma, x1, x2):
    z1 = (x1 - mu)/sigma
    z2 = (x2 - mu)/sigma
    return 0.5*(math.erf(z2/math.sqrt(2.)) - math.erf(z1/math.sqrt(2.)))

teorico = []
for n in range(10,61):
    print (n)
    teorico.append(gauss_integral(mu, sigma, n-1, n)*n_lanci)
    
chi = sum((occorrenze-teorico)**2/(teorico))
print (teorico)
print (occorrenze)
print (chi)



pylab.title('10000 lanci di 10 dadi')
pylab.xlabel('Somma delle uscite')
pylab.ylabel('Occorrenze')
pylab.grid()
pylab.bar(valori,occorrenze)
pylab.plot(valori, teorico)

pylab.show()
