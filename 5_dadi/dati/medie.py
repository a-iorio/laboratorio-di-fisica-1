import pylab
import numpy
import math

n_lanci = 1000
n_medie = 30

valori, medie = pylab.loadtxt('/Users/TheMaker/Desktop/lab/dadi/dati/25/all.txt', unpack = True)
mu = sum(medie)/n_medie
varianza_campione = sum(medie*((valori - mu)**2))/(n_medie - 1)
sigma = pylab.sqrt(varianza_campione)
occorrenze = numpy.histogram(medie)
print (occorrenze[1])

def gauss_integral(mu, sigma, x1, x2):
    z1 = (x1 - mu)/sigma
    z2 = (x2 - mu)/sigma
    return 0.5*(math.erf(z2/math.sqrt(2.)) - math.erf(z1/math.sqrt(2.)))

print (gauss_integral(3.52,0.013,3.45,3.55)*30)

pylab.grid()
pylab.hist(medie)
pylab.savefig('10.pdf')

pylab.show()
